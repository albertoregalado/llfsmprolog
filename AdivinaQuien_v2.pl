go :- hypothesize(Person),
      write('I guess the Person is: '),
      write(Person),    
      nl,
      undo.

/* hypotheses to be tested */
hypothesize(thomas)   :- thomas, !.
%hypothesize(max)     :- max, !.
hypothesize(sophie)   :- sophie, !.
%hypothesize(lucas)     :- lucas, !.
%hypothesize(philippe)   :- philippe, !.
hypothesize(joe)   :- joe, !.
hypothesize(paul) :- paul, !.
hypothesize(peter)   :- peter, !.
%hypothesize(herman)     :- herman, !.
hypothesize(charles)   :- charles, !.
hypothesize(anne)     :- anne, !.
%hypothesize(bernard)   :- bernard, !.
hypothesize(michael)   :- michael, !.
%hypothesize(roger) :- roger, !.
hypothesize(maria) :- maria, !.
%hypothesize(sarah)   :- sarah, !.
%hypothesize(theo)     :- theo, !.
%hypothesize(eric)   :- eric, !.
%hypothesize(victor)     :- victor, !.
%hypothesize(stephen)   :- stephen, !.
hypothesize(hans)   :- hans, !.
hypothesize(daniel) :- daniel, !.
hypothesize(katrin)   :- katrin, !.
%hypothesize(frank) :- frank, !.
hypothesize(unknown).             /* no diagnosis */

/* Person identification Rules */

thomas :- has_brownhair, !, has_moustache, !, has_brownskin, !.
sophie :- is_female, !, has_blackhair, !, wear_glasses, !.
katrin :- is_female, !, has_brownhair, !, wear_hat, !.
paul :- has_whitehair, !, is_male, !, has_beard, !.
charles :- has_brownhair, is_bald, wear_glasses, !.
maria :- is_female, has_redhair, wear_hat, !.
hans :- has_blondhair, !,has_moustache ,!.
peter :- has_whitehair, !, is_male, !, has_moustache, !.
anne :- is_female, has_whitehair, wear_glasses.
joe :- has_blondhair, is_male, has_blueeyes.
daniel :- is_male, is_bald, has_whitehair, wear_glasses, !.
michael :- has_brownhair, is_male, has_moustache, not(has_brownskin).
sarah:- is_female, has_blondhair, has_blueeyes.



/* clasification rules */
has_blondhair :- verify(has_blondhair), !.
has_brownhair :- verify(has_brownhair), !.
has_moustache :- verify(has_moustache), !.
has_brownskin :- verify(has_browskin), !.
is_female :- verify(is_female), !.
%is_female :- not(has_beard), not(has_moustache), !.
is_male:- not(is_female), !.
has_blackhair :- verify(has_blackhair), !.
wear_glasses :- verify(wearing_glasses), !.
wear_hat :- verify(wearing_hat), !.
has_beard :- verify(has_beard), !.
has_whitehair:- verify(has_whitehair), !.
is_bald :- verify(is_bald), !.
has_redhair :- verify(has_redhair), !.
has_blueeyes :- verify(has_blueeyes), !.


%brownguys:- thomas, sophie,bernard, max, roger.
%blondhair:- hans,sarah,joe, lucas, eric.
%redhair:- philippe, herman, stephen, maria.
%brownhair:-katrin, michael, thomas, bernard.
%blackhair:-sophie, max, theo, frank.
%moustache:- hans, peter, michael, thomas, philippe.
%beard:- lucas, max, philippe, roger, paul.
%whitehair:-paul, peter, anne, daniel, victor.
%blueyes:-charles, anne, joe, sarah, stephen.
%wearglasses:- charles, anne, daniel, sophie, stephen.
%wearhat:-katrin, maria, eric, frank, bernard.
%isbald:- charles, daniel, roger, philippe.


/* how to ask questions */
ask(Question) :-
    write('Does the person have the following attribute: '),
    write(Question),
    write('? '),
    read(Response),
    nl,
    ( (Response == yes ; Response == y) -> assert(yes(Question)) ;
	(Response == no ; Response == n) -> assert(no(Question)), fail).

:- dynamic yes/1,no/1.

/* How to verify something */
verify(S) :- (yes(S) -> true ;
    (no(S) -> fail ;
     ask(S))).

/* undo all yes/no assertions */
undo :- retract(yes(_)),fail. 
undo :- retract(no(_)),fail.
undo.


